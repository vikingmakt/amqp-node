var Publish = {
    push: function (channel, exchange, routingKey, body, properties) {
        channel.publish(
            exchange,
            routingKey,
            new Buffer(body),
            properties
        );
    }
};

module.exports = Publish;
