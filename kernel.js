var Engine = require('./engine');

function Kernel () {
    var _engines = {};

    function _engine (name, callback) {
        if (_engines[name] === undefined) {
            _engines[name] = new Engine(name);
        }
        process.nextTick(callback,_engines[name]);
    }

    function _engineOpen (name, config, callback) {
        _engine(name,function (engine) {
            _engines[name].onClose(function () {
                _engines[name] = undefined;
                process.nextTick(_engineOpen,name,config);
            });
            engine.run(config);
            if (typeof callback == 'function')
                process.nextTick(callback,engine);
        });
    }

    return {
        get engines () {
            return _engines;
        },

        engine: _engine,
        engineOpen: _engineOpen
    };
}

module.exports = Kernel;
