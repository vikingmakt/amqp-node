var Queue = {
    bind: function (channel, queue, exchange, routingKey, args, callback) {
        if (typeof args == 'function') {
            callback = args;
            args = {};
        }
        return channel.bindQueue(queue, exchange, routingKey, args)
            .then(callback);
    },
    check: function (channel, queue, callback) {
        return channel.checkQueue(queue)
            .then(callback);
    },
    declare: function (channel, queue, options, callback) {
        return channel.assertQueue(queue, options)
            .then(callback);
    },
    delete: function (channel, queue, options, callback) {
        return channel.deleteQueue(queue, options)
            .then(callback);
    },
    unbind: function (channel, queue, exchange, routingKey, args, callback) {
        if (callback === undefined) {
            callback = args;
            args = null;
        }
        return channel.unbindQueue(queue, exchange, routingKey, args)
            .then(callback);
    }
};

module.exports = Queue;
