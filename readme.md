# AMQP Node

# Summary

+ [Channel](#channel)
    + [onChannel](#onchannel)
    + [run](#run)
+ [Connection](#connection)
    + [onConnection](#onconnection)
    + [run](#run-1)
+ [Engine](#engine)
    + [channel](#channel-1)
    + [connection](#connection-1)
    + [run](#run-2)
+ [Kernel](#kernel)
    + [engine](#engine-1)
    + [engineOpen](#engineopen)

---

## Channel

### Attributes

+ **ready**: Returns if the *channel* is ready

### Methods

#### onChannel

Call a callback when *channel* is open

+ **callback**: [function(channel)] Callback that will receive the *channel*

```
channel.onChannel($callback);
```

#### run

Open a RabbitMQ channel

+ **connection**: RabbitMQ connection

```javascript
channel.run($connection);
```

---

## Connection

### Attributes

+ **ready**: Returns if the *connection* is ready

### Methods

#### onConnection

Call a callback when *connection* is open

+ **callback**: [function(connection)] Callback that will receive the *connection*

```javascript
connection.onConnection($callback);
```

#### run

Open a RabbitMQ connection

+ **config**: [object] Config to *engine*
    + **url**: [string] RabbitMQ url
    + **options**: [AMQP](https://github.com/squaremo/amqp.node) options

```javascript
connection.run($config);
```

---

## Engine

### Attributes

+ **consumer**: Returns a *consumer* actor
+ **exchange**: Returns an *exchange* actor
+ **name**: Returns the *engine's* name
+ **queue**: Returns a queue actor
+ **publish**: Returns a publish actor

### Methods

#### channel

Open a *channel*

+ **name**: [string] Name of *channel*
+ **callback**: [function(channel)] Callback that will receive the *channel*

```javascript
engine.channel($name, $callback);
```

#### connection

Call a callback when *connection* is open

+ **callback**: [function(connection)] Callback that will receive the *connection*

```javascript
engine.connection($callback);
```

#### run

Start the engine and open a RabbitMQ connection

+ **config**: [object] Config to *engine*
    + **url**: [string] RabbitMQ url
    + **options**: [AMQP](https://github.com/squaremo/amqp.node) options

```javascript
engine.run($config);
```

---

## Kernel

### Attributes

+ **engines**: Key/Value of engines

### Methods

#### engine

Open an *engine*

+ **engineName**: [string] Name of *engine*
+ **callback**: [function(engine)] Callback that will receive the *engine*

```javascript
Kernel.engine($engineName, $callback);
```

#### engineOpen

Open an *engine* and start it

+ **engineName**: [string] Name of *engine*
+ **config**: [object] Config to *engine*
    + **url**: [string] RabbitMQ url
    + **options**: [AMQP](https://github.com/squaremo/amqp.node) options
+ **callback**: [function(engine)] Callback that will receive an engine

```javascript
Kernel.engineOpen($engineName, $config, $callback);
```
