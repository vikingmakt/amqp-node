var Channel = require('./channel');
var Connection = require('./connection');
var Consumer = require('./consumer');
var Exchange = require('./exchange');
var Queue = require('./queue');
var Publish = require('./publish');

function Engine (name) {
    var _callbacks = [];
    var _channels = {};
    var _closeCallbacks = [];
    var _closed = false;
    var _connectionActor = new Connection();
    var _name = name;
    var _ready = false;
    var _running = false;

    function _channel (name, ops, callback) {
        if (typeof ops == 'function') {
            callback = ops;
            ops = {};
        }
        if (!_channels[name]) {
            _channels[name] = new Channel(_connectionActor, name, ops.prefetch);
        }
        _channels[name].onChannel(callback);
    };

    function _close () {
        if (!_closed)
            return;
        _closed = true;
        _connection(function (conn) {
            conn.close();
        });
    }

    function _connection (callback) {
        _connectionActor.onConnection(callback);
    }

    function _onClose (callback) {
        if (typeof callback != 'function')
            return;
        _closeCallbacks.push(callback);
    }

    function _onDisconnect (err) {
        if (_closed)
            return;
        while (_closeCallbacks.length > 0)
            process.nextTick(_closeCallbacks.shift());
    }

    function _make (config) {
        if (_running)
            return;
        _running = true;
        _connectionActor.run(config.url, config.options);
        _connectionActor.onConnection(function (connection) {
            connection.on('close',_onDisconnect);
        });
    }

    function _run (config) {
        process.nextTick(_make,config);
    }

    return {
        get closed () {
            return _closed;
        },
        get consumer () {
            return Consumer;
        },
        get exchange () {
            return Exchange;
        },
        get name () {
            return _name;
        },
        get queue () {
            return Queue;
        },
        get publish () {
            return Publish;
        },

        channel: _channel,
        close: _close,
        connection: _connection,
        onClose: _onClose,
        run: _run
    };
}

module.exports = Engine;
