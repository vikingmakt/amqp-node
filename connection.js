var amqp = require('amqplib');

function Connection () {
    var _callbacks = [];
    var _closed = false;
    var _connection = null;
    var _ready = false;

    function _close () {
        if (_closed)
            return;
        _closed = true;
        _onConnected(function (connection) {
            connection.close();
        });
    }

    function _make (url, options) {
        amqp.connect(url, options)
            .then(_onConnected)
            .catch(_onError);
    }

    function _onConnected (connection) {
        _connection = connection;
        console.log('MQ: Connected on ' + _connection.connection.stream._host);
        _ready = true;
        while (_callbacks.length) {
            process.nextTick(_callbacks.pop(),_connection);
        }
    }

    function _onConnection (callback) {
        if (typeof callback != 'function') {
            return;
        }
        if (_ready) {
            process.nextTick(callback,_connection);
            return;
        }
        _callbacks.push(callback);
    }

    function _onError (args) {
        console.log('MQ: ' + args);
    }

    function _run (url, options) {
        process.nextTick(_make,url,options);
    }

    return {
        get closed () {
            return _closed;
        },
        get ready () {
            return _ready;
        },

        close: _close,
        onConnection: _onConnection,
        run: _run
    };
}

module.exports = Connection;
