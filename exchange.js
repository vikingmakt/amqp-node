var Exchange = {
    bind: function (channel, dest, source, routingKey, args, callback) {
        return channel.bindExchange(dest, source, routingKey, args)
            .then(callback);
    },
    check: function (channel, exchange, callback) {
        return channel.checkExchange(exchange)
            .then(callback);
    },
    declare: function (channel, exchange, type, options, callback) {
        return channel.assertExchange(exchange, type, options)
            .then(callback);
    },
    delete: function (channel, exchange, options, callback) {
        return channel.deleteExchange(exchange, options)
            .then(callback);
    },
    unbind: function (channel, dest, source, routingKey, args, callback) {
        return channel.unbindExchange(dest, source, routingKey, args)
            .then(callback);
    }
};

module.exports = Exchange;
