var Exchange = require('../../exchange');
var FakePromise = require('../mocks/fake-promise');
var test = require('tape');

test('Exchange:bind', function (t) {
    var channel = {
        bindExchange: function (dest, source, routingKey, args) {
            t.equal(dest,'123');
            t.equal(source,'456');
            t.equal(routingKey,'789');
            t.equal(args,'02020');
            t.end();
            return FakePromise;
        }
    };
    Exchange.bind(channel,'123','456','789','02020');
});

test('Exchange:check', function (t) {
    var channel = {
        checkExchange: function (exchange) {
            t.equal(exchange,'test');
            t.end();
            return FakePromise;
        }
    };
    Exchange.check(channel,'test');
});

test('Exchange:declare', function (t) {
    var channel = {
        assertExchange: function (exchange, type, options) {
            t.equal(exchange,'test');
            t.equal(type, 'topic');
            t.equal(options, 'op');
            t.end();
            return FakePromise;
        }
    };
    Exchange.declare(channel,'test','topic','op');
});

test('Exchange:delete', function (t) {
    var channel = {
        deleteExchange: function (exchange, options) {
            t.equal(exchange,'test');
            t.equal(options, 'op');
            t.end();
            return FakePromise;
        }
    };
    Exchange.delete(channel,'test','op');
});

test('Exchange:unbind', function (t) {
    var channel = {
        unbindExchange: function (dest, source, routingKey, args) {
            t.equal(dest,'test');
            t.equal(source,'test1');
            t.equal(routingKey,'test2');
            t.equal(args,'headers');
            t.end();
            return FakePromise;
        }
    };
    Exchange.unbind(channel,'test','test1','test2','headers');
});
