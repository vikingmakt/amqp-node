var Kernel = require('../../kernel');
var test = require('tape');

test('Kernel:engine', function (t) {
    var kernel = new Kernel();

    t.equal(Object.keys(kernel.engines).length,0);
    kernel.engine('test',function (engine) {
        t.ok(engine.run);
        t.equal(Object.keys(kernel.engines).length,1);
        t.end();
    });
});

test('Kernel:engines engines should be empty', function (t) {
    var kernel = new Kernel();

    t.equal(Object.keys(kernel.engines).length,0);
    t.end();
});
