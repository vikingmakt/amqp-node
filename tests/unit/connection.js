var Connection = require('../../connection');
var test = require('tape');

test('Connection:ready connection starting not ready', function (t) {
    var conn = new Connection();

    t.equal(conn.ready,false);
    t.end();
});
