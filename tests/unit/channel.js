var Channel = require('../../channel');
var test = require('tape');

test('Channel:ready channel starting not ready', function (t) {
    var connection = {
        onConnection: function (cb) {
        }
    }
    var channel = new Channel(connection,'test');

    t.equal(channel.ready,false);
    t.end();
});
