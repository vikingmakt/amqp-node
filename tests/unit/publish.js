var Publish = require('../../publish');
var test = require('tape');

test('Publish:push', function (t) {
    var channel = {
        publish: function (exchange, routingKey, body, properties) {
            t.equal(exchange,'test');
            t.equal(routingKey,'test1');
            t.equal(body.toString(),'test2');
            t.equal(properties,'prop');
            t.end();
        }
    };
    Publish.push(channel,'test','test1','test2','prop');
});
