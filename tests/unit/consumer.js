var Consumer = require('../../consumer');
var test = require('tape');

test('Consumer:cancel', function (t) {
    var channel = {
        cancel: function (consumerTag) {
            t.equal(consumerTag,'tag');
            t.end();
        }
    };
    Consumer.cancel(channel,'tag');
});

test('Consumer:consume', function (t) {
    var channel = {
        consume: function (queueName, consumerCallback, options) {
            t.equal(queueName,'test');
            t.equal(consumerCallback,'test2');
            t.equal(options,'op');
            t.end();
        }
    };
    Consumer.consume(channel, 'test', 'test2', 'op');
});
