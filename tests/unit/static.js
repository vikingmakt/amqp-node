var sha1 = require('sha1');
var test = require('tape');
var Static = require('../../static');

test('Static:channelName', function (t) {
    var expected = sha1('name:15');
    var result = Static.channelName('name','15');

    t.equal(result,expected);
    t.end();
});

test('Static:consumerTag', function (t) {
    var expected = 'tag:' + sha1('<consumer>::1245');
    var result = Static.consumerTag('1245');

    t.equal(result,expected);
    t.end();
});

test('Static:queueName', function (t) {
    var expected = 'tornpack_actor_rmq_123456_' + sha1('123456');
    var result = Static.queueName('123456');

    t.equal(result,expected);
    t.end();
});

test('Static:routingKey', function (t) {
    var expected = 'tornpack.actor.rmq.' + sha1('123456');
    var result = Static.routingKey('123456');

    t.equal(result,expected);
    t.end();
});
