var FakePromise = require('../mocks/fake-promise');
var Queue = require('../../queue');
var test = require('tape');

test('Queue:bind bind with args', function (t) {
    var channel = {
        bindQueue: function (queue, exchange, routingKey, args) {
            t.equal(queue,'test');
            t.equal(exchange,'test1');
            t.equal(routingKey,'');
            t.equal(args.test,true);
            t.end();
            return FakePromise;
        }
    };
    Queue.bind(channel,'test','test1','',{test:true},false);
});

test('Queue:bind bind without args', function (t) {
    var channel = {
        bindQueue: function (queue, exchange, routingKey, args) {
            t.equal(queue,'test');
            t.equal(exchange,'test1');
            t.equal(routingKey,'test2');
            t.equal(args,null);
            t.end();
            return FakePromise;
        }
    };
    Queue.bind(channel,'test','test1','test2');
});

test('Queue:check', function (t) {
    var channel = {
        checkQueue: function (queue) {
            t.equal(queue,'test');
            t.end();
            return FakePromise;
        }
    };
    Queue.check(channel,'test');
});

test('Queue:declare', function (t) {
    var channel = {
        assertQueue: function (queue, options) {
            t.equal(queue,'test');
            t.equal(options,'op');
            t.end();
            return FakePromise;
        }
    };
    Queue.declare(channel,'test','op');
});

test('Queue:delete', function (t) {
    var channel = {
        deleteQueue: function (queue, options) {
            t.equal(queue,'test');
            t.equal(options,'op');
            t.end();
            return FakePromise;
        }
    };
    Queue.delete(channel,'test','op');
});

test('Queue:unbind unbind with args', function (t) {
    var channel = {
        unbindQueue: function (queue, exchange, routingKey, args) {
            t.equal(queue,'test');
            t.equal(exchange,'test1');
            t.equal(routingKey,'');
            t.equal(args.test,true);
            t.end();
            return FakePromise;
        }
    };
    Queue.unbind(channel,'test','test1','',{test:true},false);
});

test('Queue:unbind unbind without args', function (t) {
    var channel = {
        unbindQueue: function (queue, exchange, routingKey, args) {
            t.equal(queue,'test');
            t.equal(exchange,'test1');
            t.equal(routingKey,'test2');
            t.equal(args,null);
            t.end();
            return FakePromise;
        }
    };
    Queue.unbind(channel,'test','test1','test2');
});
