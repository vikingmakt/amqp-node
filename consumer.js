var Consumer = {
    cancel: function (channel, consumerTag) {
        process.nextTick(channel.cancel.bind(channel),consumerTag);
    },
    consume: function (channel, queueName, consumerCallback, options) {
        process.nextTick(channel.consume.bind(channel),queueName,consumerCallback,options);
    }
}

module.exports = Consumer;
