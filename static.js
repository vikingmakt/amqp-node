var sha1 = require('sha1');

function channelName(name,uid) {
    return sha1(name + ':' + uid)
}

function consumerTag (_) {
    return 'tag:' + sha1('<consumer>::' + _);
}

function queueName (_) {
    return 'tornpack_actor_rmq_' + _ + '_' + sha1(_);
}

function routingKey(_) {
    return 'tornpack.actor.rmq.' + sha1(_);
}

module.exports = {
    channelName: channelName,
    consumerTag: consumerTag,
    queueName: queueName,
    routingKey: routingKey
}
