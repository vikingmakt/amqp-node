var Config = require('./config');

function Channel (connection, name, prefetch) {
    var _callbacks = [];
    var _channel = null;
    var _name = name;
    var _prefetch = prefetch || Config.rabbitmqPrefetch;
    var _ready = false;

    function _onChannel (callback) {
        if (_ready) {
            process.nextTick(callback,_channel);
            return;
        }
        _callbacks.push(callback);
    }

    function _onChannelCreated (channel) {
        _channel = channel;
        _channel.prefetch(_prefetch);
        _ready = true;
        while (_callbacks.length) {
            process.nextTick(_callbacks.pop(),_channel);
        }
    }

    function _onConnection (connection) {
        connection.createChannel().then(_onChannelCreated);
    }

    connection.onConnection(_onConnection);

    return {
        get io () {
            return _channel;
        },

        get prefetch () {
            return _prefetch;
        },

        get ready () {
            return _ready;
        },

        onChannel: _onChannel
    };
}

module.exports = Channel;
